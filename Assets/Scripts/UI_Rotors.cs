﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UI_Rotors : MonoBehaviour
{

	public GameObject rotor;
	private MotorScript script;
	private const float maxpower = 20f;
	private float step, thr;
	private Image image;

	// Use this for initialization
	void Start ()
	{
		script = rotor.GetComponent <MotorScript> ();
		image = GetComponent <Image> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		thr = Mathf.Abs (script.throttle);
		step = thr / maxpower;
		var value = 255 * step > 255 ? 255 : 255 * step;
		image.color = new Color (step, 192, 0, 255);

	}
}
