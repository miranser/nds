﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControllerScript : MonoBehaviour
{
	private Queue CamList;
	public Camera[] clist;
	// Use this for initialization
	void Start ()
	{
		CamList = new Queue ();
		foreach (var item in clist) {
			CamList.Enqueue (item);
		}
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		
		if (Input.GetKeyDown ("joystick button 7")) {
			Camera.current.enabled = false;
			NextCam ().enabled = true;
			//print (c);
		}
	}

	private Camera NextCam ()
	{
		Camera next = (Camera)CamList.Dequeue ();
		//next.enabled = false;
		CamList.Enqueue (next);
		return next;
	}

}
