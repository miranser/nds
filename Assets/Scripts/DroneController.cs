﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneController : MonoBehaviour
{
	
	public float pitch, yaw, roll, latitude, longtitude, altitude;
	public float targetPitch, targetYaw, targetRoll, targetLatitude, targetLongtitude, targetAltitude;
	public float power;
	public float Ppitch, Ipitch, Dpitch, Pyaw, Iyaw, Dyaw, Proll, Iroll, Droll, Palt, Ialt, Dalt, Plat, Ilat, Dlat, Plong, Ilong, Dlong;
	private PID PIDpitch, PIDyaw, PIDroll, PIDlatitude, PIDlongtitude, PIDaltitude;
	public float powerLimit, angleLimit;
	private MotorScript frRotor, flRotor, blRotor, brRotor;
	private float velocity;

	private float frontLeftMotorPower, frontRightMotorPower, rearLeftMotorPower, rearRightMotorPower;
	private float pitchForce, rollForce, yawForce, altForce, forceLimit;
	private float dPitch, dRoll, dYaw, dAlt, dLat, dLong;
	private bool[] pitchSeq = { true, true, false, false };
	private bool[] rollSeq = { true, false, true, false };
	private bool[] yawSeq = { false, true, true, false };
	private bool[] altSeq = { true, true, true, true };
	Controls Control;
	public bool Movetop = false;

	public float Power {
		get {
			return power;
		}
		set {
			power = value < 0 ? 0 : value;
			power = value > powerLimit * 2 ? powerLimit : value;
		}
	}
	// Use this for initialization
	void Start ()
	{
		
		Control = GetComponent<Controls> ();
		forceLimit = power > powerLimit ? powerLimit : power;

		frRotor = GameObject.Find ("frontRightMotor").GetComponent <MotorScript> ();
		flRotor = GameObject.Find ("frontLeftMotor").GetComponent <MotorScript> ();
		brRotor = GameObject.Find ("rearRightMotor").GetComponent <MotorScript> ();
		blRotor = GameObject.Find ("rearLeftMotor").GetComponent <MotorScript> ();

		PIDpitch = new PID (Ppitch, Ipitch, Dpitch);
		PIDyaw = new PID (Pyaw, Iyaw, Dyaw);
		PIDroll = new PID (Proll, Iroll, Droll);
		PIDlatitude = new PID (Plat, Ilat, Dlat);
		PIDlongtitude = new PID (Plong, Ilong, Dlong);
		PIDaltitude = new PID (Palt, Ialt, Dalt);

	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		GetRotations ();
		Stabilize ();
		GetComponent <TextMesh> ().text = "power: " + power + "\npitch: " + pitch + "\nroll: " + roll + "\nyaw: " + yaw + "\nSpeed: " + velocity;

		/*if (Movetop)
			MoveToPoint (new Vector3 (60, 10, 0));*/

	}

	public void GetRotations ()
	{
		var rot = transform;
		pitch = rot.rotation.eulerAngles.x;
		roll = rot.rotation.eulerAngles.z;
		yaw = rot.rotation.eulerAngles.y;
		latitude = transform.position.z;
		longtitude = transform.position.x;
		altitude = transform.position.y;

	}

	private void FourMotorsForce (float power)
	{

		frRotor.throttle = power;
		flRotor.throttle = power;
		blRotor.throttle = power;
		brRotor.throttle = power;
	}

	public void rotateToPoint ()
	{
		targetYaw = yawDifference (Control.point);
	}

	public float yawDifference (Vector3 point)
	{
		Vector3 vect = (transform.position - point).normalized;
		var q = Quaternion.LookRotation (vect);
		return q.eulerAngles.y - 180;
	}

	public void MoveToPoint (Vector3 point)
	{
		var distance = Vector3.Distance (transform.position, point);
		print (distance);
		if (yawDifference (point) > 0.5)
			rotateToPoint ();
		else {
			if (distance > 10)
				targetPitch = 15;
			else
				Brake ();
		}
	}

	private void Brake ()
	{
		targetPitch = velocity > 0.5 ? -15 : 0;
	}

	private float stabilizePitch ()
	{
		dPitch = targetPitch - pitch;

		dPitch -= (float)(Mathf.Ceil (Mathf.Floor (dPitch / 180) / 2) * 360);

		float pitchForce = -PIDpitch.Calculate (0, dPitch / 180);
		pitchForce = pitchForce > forceLimit ? forceLimit : pitchForce;
		pitchForce = pitchForce < -forceLimit ? -forceLimit : pitchForce;

		return pitchForce;
	}

	private float stabilizeRoll ()
	{
		dRoll = targetRoll - roll;

		dRoll -= (float)Mathf.Ceil (Mathf.Floor (dRoll / 180) / 2) * 360;

		float rollForce = -PIDroll.Calculate (0, dRoll / 180);
		rollForce = rollForce > forceLimit ? forceLimit : rollForce;
		rollForce = rollForce < -forceLimit ? -forceLimit : rollForce;

		return rollForce;
	}

	private float stabilizeYaw ()
	{
		dYaw = targetYaw - yaw;

		dYaw -= (float)Mathf.Ceil (Mathf.Floor (dYaw / 180) / 2) * 360;

		float yawForce = PIDyaw.Calculate (0, dYaw / 180);
		yawForce = yawForce > forceLimit ? forceLimit : yawForce;
		yawForce = yawForce < -forceLimit ? -forceLimit : yawForce;

		return yawForce;
	}

	private float stabilizeAlt ()
	{
		dAlt = targetAltitude - altitude;

		float altForce = PIDaltitude.Calculate (0, dAlt);
		altForce = altForce > forceLimit ? forceLimit : altForce;
		altForce = altForce < -forceLimit ? -forceLimit : altForce;

		return altForce;
	}

	/*private float stabilzeLat ()
	{
		

	}*/

	private void Stabilize ()
	{
		frontLeftMotorPower = power;
		frontRightMotorPower = power;
		rearLeftMotorPower = power;
		rearRightMotorPower = power;


		pitchForce = stabilizePitch ();
		rotorsPower (pitchForce, pitchSeq);

		rollForce = stabilizeRoll ();
		rotorsPower (rollForce, rollSeq);

		yawForce = stabilizeYaw ();
		rotorsPower (yawForce, yawSeq);

		altForce = stabilizeAlt ();
		rotorsPower (altForce, altSeq);

		frRotor.throttle = frontRightMotorPower;
		flRotor.throttle = -frontLeftMotorPower;
		brRotor.throttle = -rearRightMotorPower;
		blRotor.throttle = rearLeftMotorPower;

	}

	private void rotorsPower (float pitchForce, bool[]seq)
	{
		frontLeftMotorPower += seq [0] ? pitchForce : -pitchForce;
		frontRightMotorPower += seq [1] ? pitchForce : -pitchForce;
		rearLeftMotorPower += seq [2] ? pitchForce : -pitchForce;
		rearRightMotorPower += seq [3] ? pitchForce : -pitchForce;
	}

}
