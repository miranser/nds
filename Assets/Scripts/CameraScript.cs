﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

	public GameObject drone;
	public float xDistance, yDistance, zDistance, xRotation, yRotation, zRotation;
	private Vector3 offset;

	public bool IsGameMode{ get; set; }

	public bool IsPathEditorMode{ get; set; }

	public Vector3 PEpos;
	// Use this for initialization
	void Start ()
	{
		IsGameMode = true;
		IsPathEditorMode = false;
		offset = new Vector3 (xDistance, yDistance, zDistance);
		PEpos = new Vector3 (drone.transform.position.x, 100, drone.transform.position.z);
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		//transform.position = drone.transform.TransformPoint (offset);

		if (IsGameMode)
			GameMode ();
		if (IsPathEditorMode) {
			//transform.position = Vector3.mo
//			transform.position = Vector3.Lerp (transform.position, new Vector3 (drone.transform.position.x, 100, drone.transform.position.z), 2);
		}
		//PathEditorMode ();
		
		transform.LookAt (drone.transform);



		/*transform.position = drone.transform.TransformPoint (offset);
		//transform.RotateAround (drone.transform.position, Vector3.up, drone.transform.rotation.eulerAngles.z);
		//transform.rotation = Quaternion.Euler (xRotation, , zRotation);
		transform.LookAt (drone.transform);
		//transform.eulerAngles = new Vector3 (45, transform.eulerAngles.y, 0);*/
	}

	public void GameMode ()
	{
		transform.position = drone.transform.TransformPoint (offset);
		//transform.RotateAround (drone.transform.position, Vector3.up, drone.transform.rotation.eulerAngles.z);
		//transform.rotation = Quaternion.Euler (xRotation, , zRotation);

		//transform.eulerAngles = new Vector3 (45, transform.eulerAngles.y, 0);
	}

	public void PathEditorMode ()
	{
		//transform.position = new Vector3 (drone.transform.position.x, 100, drone.transform.position.z);
		transform.position = Vector3.Lerp (transform.position, new Vector3 (drone.transform.position.x, 100, drone.transform.position.z), 2);
		/*transform.rotation = Quaternion.Euler (90, 0, 0);*/
	}

}
