﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{

	// Use this for initialization
	public void ResumeButton ()
	{
		GameObject.Find ("PausePanel").gameObject.SetActive (false);
		Time.timeScale = 1;
	}
}
