﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Controls : MonoBehaviour
{

	// Use this for initialization

	public Vector3 point;
	public GameObject targetPoint;
	public Quaternion qqq;
	public DroneController Controller;
	public CameraScript CameraController;
	public Transform canvas;


	void Start ()
	{
		
		Controller = GetComponent <DroneController> ();
		//CameraController = GameObject.Find ("MainCamera").GetComponent <CameraScript> ();

	}


	// Update is called once per frame
	void FixedUpdate ()
	{
		/*if (Input.GetMouseButton (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				point = hit.point;
				targetPoint.transform.position = point;
				Controller.MoveToPoint (point);
			}
		}*/

		if (Mathf.Abs (Input.GetAxis ("RyAxis")) > 0.1) {
			Controller.targetAltitude += Input.GetAxis ("RyAxis") * -1.25f * Time.fixedDeltaTime;
			/*Controller.power = Controller.power < 0 ? 0 : Controller.power;
			Controller.power = Controller.power > Controller.powerLimit * 2 ? Controller.powerLimit * 2 : Controller.power;*/
			//Controller.targetAltitude = Controller.altitude;
		} else
			Controller.power = 10.0f;

		if (Mathf.Abs (Input.GetAxis ("RxAxis")) > 0.01) {
			Controller.targetYaw += Input.GetAxis ("RxAxis") * 12.5f * 20 * Time.fixedDeltaTime;
		}
		if (Mathf.Abs (Input.GetAxis ("LyAxis")) > 0.1) {
		
			Controller.targetPitch = Input.GetAxis ("LyAxis") * -15;

		} else
			Controller.targetPitch = 0;
		if (Mathf.Abs (Input.GetAxis ("LxAxis")) > 0.1) {
			Controller.targetRoll = Input.GetAxis ("LxAxis") * -15;

		} else
			Controller.targetRoll = 0;

		if (Input.GetKey ("joystick button 0")) {
			Restart ();
			//Time.timeScale = 0;
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			GamePause ();
		}

	}

	public void GamePause ()
	{
		if (canvas.gameObject.activeInHierarchy == false) {
			Time.timeScale = 0;
			canvas.gameObject.SetActive (true);
		} else {
			Time.timeScale = 1;
			canvas.gameObject.SetActive (false);
		}
	}

	public void Restart ()
	{
		SceneManager.LoadScene (1);
		Time.timeScale = 1;
	}

	public void Exit ()
	{
		Application.Quit ();
	}

	private void PrintAxis ()
	{
		print ("RX: " + Input.GetAxis ("RxAxis") + "\t" + "RY: " +
		Input.GetAxis ("RyAxis") + "\t" + "LX: " +
		Input.GetAxis ("LxAxis") + "\t" + "LY: " +
		Input.GetAxis ("LyAxis") + "\t");
	}
}




