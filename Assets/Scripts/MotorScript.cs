﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorScript : MonoBehaviour
{

	public float throttle = 0.0f;
	private float power;
	private float torque;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		power = (float)Mathf.Abs (throttle);
		torque = throttle;

		GetComponent <Rigidbody> ().AddRelativeForce (0, power, 0, ForceMode.Force);
		GetComponent <Rigidbody> ().AddRelativeTorque (0, torque, 0, ForceMode.Force);

		GetComponent <TextMesh> ().text = power.ToString ("F1");
	}
}
